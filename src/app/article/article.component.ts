import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss']
})
export class ArticleComponent implements OnInit {

  @Input() article = {
    title: 'txt',
    content: 'txt',
    created_at: new Date(),
    loveIts: 0
  };

  @Input() indice: number;

  constructor() {
  }

  ngOnInit() {
  }

  onLoveIts() {
    this.article.loveIts += 1;
  }

  onDontLoveIts() {
    this.article.loveIts -= 1;
  }

  getColor() {
    if (this.article.loveIts > 0) {
      return 'green';
    } else if (this.article.loveIts < 0) {
      return 'red';
    }
  }

}
