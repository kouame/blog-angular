import {Component} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Blog';

  posts = [
    {
      title: 'Savoir manger',
      content: 'Savoir manger est une qualité importante dans la sociabilité d\'un homme',
      loveIts: 0,
      created_at: new Date('July 21, 2031 03:12:46')
    },
    {
      title: 'Savoir dormir',
      content: 'Dormir sans ronfler est une qualité importante dans la sociabilité d\'un homme',
      loveIts: 2,
      created_at: new Date('March 21, 2009 01:56:07')
    },
    {
      title: 'Faire l\'amour',
      content: 'Savoir faire l\'amour est une qualité importante dans la quete de nouvelle chatte',
      loveIts: -2,
      created_at: new Date('June 8, 2013 09:45:00')
    },
  ];

}
